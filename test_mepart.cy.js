const uuid = (max) => Math.floor(Math.random()*max)
const ppid = uuid(90000000000)
const testname = `testname${ppid}`
const testphone = `${ppid}`
const testmail = `testmail${ppid}`

Cypress.on('uncaught:exception', (err, runnable) => {
  return false;
});

describe('test case for test', () => {

  beforeEach(() => {
    cy.visit('https://dev.mepart.ru/personal/cart/')
    cy.viewport(1903, 961);
  });

it('buying test', () => {

  cy.get('#input-autocomplit')
    .click()
    .type('A113707110CA')

  cy.get('#input-autocomplit-submit > img')
    .click()
    cy.contains('CHERY')
    .click()

  cy.get(':nth-child(2) > .search_product_buy > .btn > img')
    .click()

  cy.get('#basket-add-popup-submit')
    .click()

  cy.get('.cart-link > img')
    .click()

  cy.get('.d-flex > .red-btn')
    .click()

  cy.get('.checkout__content > :nth-child(1) > .form-control')
    .type(testname)

  cy.get('.checkout__content > :nth-child(2) > .form-control')
    .type(testphone)

  cy.get('.checkout__content > :nth-child(3) > .form-control')
    .type(testmail+'@gmail.com')

  cy.get('#bx_a28af37bd142ae0d7e2e178d49544353')
    .click({force: true})

  cy.get('#PAY_SYSTEM_4')
    .click({force: true})

  cy.get('.my-2 > .px-md-0 > .form-control')
    .click()
    

})

})
